
from django.urls import path, include
from rest_framework.routers import DefaultRouter
# from inmuebleslist_app.api.views import inmueble_list, inmueble_detalle
from inmuebleslist_app.api.views import EdificacionListAV, EdificacionDetalleAV, EmpresaAV, EmpresaDetalleAV, ComentarioList, ComentarioDetail, EmpresaVS 

router = DefaultRouter()
router.register('empresa', EmpresaVS, basename='empresa')

urlpatterns = [
    
    # urls de vistas basadas en funciones
    # path("list/", inmueble_list, name="inmueble-list"),
    # path("<int:pk>/", inmueble_detalle, name="inmueble-detalle"),
    
    # urls de vistas basadas en clases
    path("edificacion/", EdificacionListAV.as_view(), name="edificacion-list"),
    path("edificacion/<int:pk>/", EdificacionDetalleAV.as_view(), name="edificacion-detalle"),
    
    path("", include(router.urls)),
    # path("empresa/", EmpresaAV.as_view(), name="empresa"),
    # path("empresa/<int:pk>/", EmpresaDetalleAV.as_view(), name="empresa-detail"),
    
    path("edificacion/<int:pk>/comentario/", EdificacionDetalleAV.as_view(), name="comentario-list"),
    path("edificacion/comentario/<int:pk>/", ComentarioDetail.as_view(), name="comentario-detail")
]
