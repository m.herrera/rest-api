
from inmuebleslist_app.models import Edificacion, Empresa, Comentario
from inmuebleslist_app.api.serializers import EdificacionSerializer, EmpresaSerializer, ComentarioSerializer
from rest_framework.response import Response
# from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework.views import APIView
from rest_framework import generics, mixins
from rest_framework.viewsets import ViewSet
from django.shortcuts import get_object_or_404

# USO DE VISTAS GENERICAS
class ComentarioList(mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView):
    queryset = Comentario.objects.all()
    serializer_class = ComentarioSerializer
    
    # devuelve la lista de tolos los comentarios
    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
    
    # envia un comentario
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)
    
class ComentarioDetail(mixins.RetrieveModelMixin, generics.GenericAPIView):
    queryset = Comentario.objects.all()
    serializer_class = ComentarioSerializer
    
    # obtiene un comentario atraves de su id(El cual va en el objeto request, en la url)
    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)
    
    

# USO DE VISTAS BASADAS EN CLASES  
class EdificacionListAV(APIView):
    
    def get(self, request):
        edificaciones = Edificacion.objects.all()
        serializer = EdificacionSerializer(edificaciones, many=True)
        return Response(serializer.data)
    
    def post(self, request):
        serializer = EdificacionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
class EdificacionDetalleAV(APIView):
    
    def get(self, request, pk):
        try:
            edificacion = Edificacion.objects.get(pk=pk)
        except Edificacion.DoesNotExist:
            return Response({'Error': 'Edificacion no encontrado'}, status=status.HTTP_404_NOT_FOUND)            
        
        serializer = EdificacionSerializer(edificacion)
        return Response(serializer.data)
    
    def put(self, request, pk):
        try:
            edificacion = Edificacion.objects.get(pk=pk)
        except Edificacion.DoesNotExist:
            return Response({'Error': 'Edificacion no encontrado'}, status=status.HTTP_404_NOT_FOUND) 
        
        serializer = EdificacionSerializer(edificacion, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
    def delete(self, request, pk):
        try:
            edificacion = Edificacion.objects.get(pk=pk)
        except Edificacion.DoesNotExist:
            return Response({'Error': 'Edificacion no encontrado'}, status=status.HTTP_404_NOT_FOUND) 
        
        edificacion.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    
# USO DE DECORADORES 
# @api_view(['GET', 'POST'])
# def inmueble_list(request):
#     if request.method == 'GET':
#         inmuebles = Inmueble.objects.all()
#         serializer = InmuebleSerializer(inmuebles, many=True)
        
#         return Response(serializer.data)
    
#     if request.method == 'POST':
#         de_serializer = InmuebleSerializer(data=request.data)
#         if de_serializer.is_valid():
#             de_serializer.save()
#             return Response(de_serializer.data, status=201)
#         else:
#             return Response(de_serializer.errors)

# @api_view(['GET', 'PUT', 'DELETE']) 
# def inmueble_detalle(request, pk):
#     # OBTENER 1 INMUEBLE
#     if request.method == 'GET':
#         try:
#             inmueble = Inmueble.objects.get(pk=pk)
#             serializer = InmuebleSerializer(inmueble)
            
#             return Response(serializer.data)          
#         except Inmueble.DoesNotExist:
#             return Response({'Error:' 'El mueble no existe'}, status=status.HTTP_404_NOT_FOUND)
           
#     # ACTUALIZAR
#     if request.method == 'PUT':
#         inmueble = Inmueble.objects.get(pk=pk)
#         de_serializer = InmuebleSerializer(inmueble, data=request.data)
#         if de_serializer.is_valid():
#             de_serializer.save()
#             return Response(de_serializer.data)
#         else:
#             return Response(de_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
#     # ELIMINAR
#     if request.method == 'DELETE':
#         try:
#             inmueble = Inmueble.objects.get(pk=pk)
#             inmueble.delete()
#         except Inmueble.DoesNotExist:
#             return Response({'Error': 'El Inmueble no existe'}, status=status.HTTP_404_NOT_FOUND)
        
#         return Response(status=status.HTTP_204_NO_CONTENT)
   
class EmpresaVS(ViewSet):
    def list(self, request):
        # devuelve la lista de todas las empresas
        queryset = Empresa.objects.all()
        serializer = EmpresaSerializer(queryset, many=True)
        return Response(serializer.data)
    
    def retrieve(self, request, pk=None):
        queryset = Empresa.objects.all()
        edificacionList = get_object_or_404(queryset, pk=pk)
        serializer = EmpresaSerializer(edificacionList)
        return Response(serializer.data)


# USO DE VISTAS BASADAS EN CLASES        
class EmpresaAV(APIView):
    
    def get(self, request):
        empresas = Empresa.objects.all()
        serializer = EmpresaSerializer(empresas, many=True, context={'request': request})
        return Response(serializer.data)
    
    def post(self, request):
        # envia los datos en json y los parsea a datos nativos de python
        serializer = EmpresaSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
class EmpresaDetalleAV(APIView):
    
    def get(self, request, pk):
        # devuelve una empresa en especifico
        try:
            empresa = Empresa.objects.get(pk=pk)
        except Empresa.DoesNotExist:
            return Response({'error': 'Empresa no encontrada'}, status=status.HTTP_404_NOT_FOUND)
        
        
        serializer = EmpresaSerializer(empresa, context={'request': request})
        return Response(serializer.data)
    
    def put(self, request, pk):
        # actualiza la empresa
        try:
            empresa = Empresa.objects.get(pk=pk)
        except Empresa.DoesNotExist:
            return Response({'error': 'Empresa no encontrada'}, status=status.HTTP_404_NOT_FOUND)
        
        serializer = EmpresaSerializer(empresa, data=request.data, context={'request': request})
        if serializer.is_valid(): # si la data enviada es correcta
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
    def delete(self, request, pk):
        # eliminamos una empresa
        try:
            empresa = Empresa.objects.get(pk=pk)
        except Empresa.DoesNotExist:
            return Response({'error': 'Empresa no encontrada'}, status=status.HTTP_404_NOT_FOUND)
        
        empresa.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
            