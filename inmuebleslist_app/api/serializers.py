
from rest_framework import serializers
from inmuebleslist_app.models import Edificacion, Empresa, Comentario

"""
Serializers de App Inmuebles
/////////////////////////////////////////////////////////////////////////////////
"""

class ComentarioSerializer(serializers.ModelSerializer):
    """ transforma los datos del modelo/tabla comentario a json/xml"""
    class Meta: 
        model = Comentario
        fields = '__all__'

class EdificacionSerializer(serializers.ModelSerializer):
    """ define la representacion de la API - transforma los datos del modelo/tabla inmueble a json/xml"""
    comentarios = ComentarioSerializer(many=True, read_only=True)
    class Meta:
        model = Edificacion
        fields = '__all__'
        # fields = ['id', 'pais', 'active', 'imagen']
        # exclude = ['id']    
        
    # def get_longitud_direccion(self, object):
    #     cantidad_caracteres = len(object.direccion)
    #     return cantidad_caracteres
        
    # def validate(self, data):
    #     if data['direccion'] == data['pais']:
    #         raise serializers.ValidationError("La direccion y el pais no pueden ser iguales")
    #     else:
    #         return data
        
    # def validate_imagen(self, data):
    #     if len(data) < 4:
    #         raise serializers.ValidationError("El valor es demasiado corto")
    #     else:
    #         return data

# def columna_longitud(value):
#     if len(value) < 4:
#         raise serializers.ValidationError("El valor es demasiado corto")

# class InmuebleSerializer(serializers.Serializer):
#     """ define la representacion de la API - transforma los datos del modelo/tabla inmueble a json/xml"""
#     id = serializers.IntegerField(read_only=True)
#     direccion = serializers.CharField(validators=[columna_longitud])
#     pais = serializers.CharField(validators=[columna_longitud])
#     descripcion = serializers.CharField()
#     imagen = serializers.CharField()
#     active = serializers.BooleanField()
    
#     def create(self, validated_data):
#         return Inmueble.objects.create(**validated_data)
    
#     def update(self, instance, validated_data):
#         instance.direccion = validated_data.get('direccion', instance.direccion)
#         instance.pais = validated_data.get('pais', instance.pais)
#         instance.descripcion = validated_data.get('descripcion', instance.descripcion)
#         instance.imagen = validated_data.get('imagen', instance.imagen)
#         instance.active = validated_data.get('active', instance.active)
#         instance.save()
#         return instance
    
#     def validate(self, data):
#         if data['direccion'] == data['pais']:
#             raise serializers.ValidationError("La direccion y el pais no pueden ser iguales")
#         else:
#             return data
        
#     def validate_imagen(self, data):
#         if len(data) < 4:
#             raise serializers.ValidationError("El valor es demasiado corto")
#         else:
#             return data

class EmpresaSerializer(serializers.ModelSerializer):
    # devuelve todos los objetos de inmuebles
    edificacionlist = EdificacionSerializer(many=True, read_only=True)
    
    # devuelve coleccion de tipo String con data 
    #edificacionlist = serializers.StringRelatedField(many=True)
    
    # devuelve el id
    #edificacionlist = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    
    # devuelve la URL de cada inmueble perteneciente a la empresa
    # edificacionlist = serializers.HyperlinkedRelatedField(
    #     many=True,
    #     read_only=True,
    #     view_name = 'edificacion-detalle' 
    # )
    class Meta:
        model = Empresa
        fields = '__all__'
    
