
### INSTALAR DRF
* pip install djangorestframework
* pip install markdown       # Markdown support for the browsable API.
* pip install django-filter  # Filtering support

### AGREGAR LA APP
```
INSTALLED_APPS = [
    'rest_framework',
]
```

***

### CODIGOS HTTP
Códigos de estado 2xx (Éxito):
* 200 OK: Solicitud exitosa. Se usa, por ejemplo, cuando un recurso se obtiene correctamente.
* 201 Created: Recurso creado exitosamente (ej. POST para crear un recurso).
* 202 Accepted: La solicitud ha sido aceptada para procesamiento, pero aún no se ha completado.
* 204 No Content: Solicitud procesada correctamente, pero no hay contenido para devolver (por ejemplo, DELETE exitoso).

Códigos de estado 3xx (Redirecciones):
* 301 Moved Permanently: El recurso solicitado ha sido movido permanentemente a una nueva URL.
* 302 Found: Redirección temporal.
* 304 Not Modified: El recurso no ha cambiado desde la última solicitud (usado con caché).

Códigos de estado 4xx (Errores del cliente):
* 400 Bad Request: La solicitud no es válida (ej. datos incorrectos en el cuerpo).
* 401 Unauthorized: El cliente no está autenticado o no tiene permisos válidos.
* 403 Forbidden: El cliente está autenticado, pero no tiene permisos para acceder al recurso.
* 404 Not Found: El recurso solicitado no existe.
* 405 Method Not Allowed: El método HTTP utilizado no es permitido para este recurso.
* 409 Conflict: Conflicto con el estado actual del recurso (ej. al intentar crear un recurso duplicado).
* 422 Unprocessable Entity: Datos válidos sintácticamente, pero semánticamente erróneos.

Códigos de estado 5xx (Errores del servidor):
* 500 Internal Server Error: Error genérico del servidor.
* 501 Not Implemented: La funcionalidad solicitada no está * implementada en el servidor.
* 502 Bad Gateway: El servidor, actuando como una puerta de enlace, recibió una respuesta inválida.
* 503 Service Unavailable: El servidor no está disponible (por mantenimiento o sobrecarga).
* 504 Gateway Timeout: El servidor no recibió una respuesta a tiempo de un servicio externo.

***

### FORMA DE SERIALIZAR UUN MODELO
```
class ComentarioSerializer(serializers.ModelSerializer):
    """ transforma los datos del modelo/tabla comentario a json/xml"""
    class Meta: 
        model = Comentario
        fields = '__all__'

class EdificacionSerializer(serializers.ModelSerializer):
    """ define la representacion de la API - transforma los datos del modelo/tabla inmueble a json/xml"""
    comentarios = ComentarioSerializer(many=True, read_only=True)
    class Meta:
        model = Edificacion
        fields = '__all__'
```

### USO DE VISTAS GENERICAS
```
class ComentarioList(mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView):
    queryset = Comentario.objects.all()
    serializer_class = ComentarioSerializer
    
    # devuelve la lista de todos los comentarios
    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
    
    # envia un comentario
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

class ComentarioDetail(mixins.RetrieveModelMixin, generics.GenericAPIView):
    queryset = Comentario.objects.all()
    serializer_class = ComentarioSerializer
    
    # obtiene un comentario atraves de su id(El cual va en el objeto request, en la url)
    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)
```

### USO DE VISTAS BASADAS EN CLASES
```
class EmpresaAV(APIView):
    
    def get(self, request):
        empresas = Empresa.objects.all()
        serializer = EmpresaSerializer(empresas, many=True, context={'request': request})
        return Response(serializer.data)
    
    def post(self, request):
        # envia los datos en json y los parsea a datos nativos de python
        serializer = EmpresaSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
class EmpresaDetalleAV(APIView):
    
    def get(self, request, pk):
        # devuelve una empresa en especifico
        try:
            empresa = Empresa.objects.get(pk=pk)
        except Empresa.DoesNotExist:
            return Response({'error': 'Empresa no encontrada'}, status=status.HTTP_404_NOT_FOUND)
        
        
        serializer = EmpresaSerializer(empresa, context={'request': request})
        return Response(serializer.data)
    
    def put(self, request, pk):
        # actualiza la empresa en especifico
        try:
            empresa = Empresa.objects.get(pk=pk)
        except Empresa.DoesNotExist:
            return Response({'error': 'Empresa no encontrada'}, status=status.HTTP_404_NOT_FOUND)
        
        serializer = EmpresaSerializer(empresa, data=request.data, context={'request': request})
        if serializer.is_valid(): # si la data enviada es correcta
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
    def delete(self, request, pk):
        # eliminamos una empresa en especifico
        try:
            empresa = Empresa.objects.get(pk=pk)
        except Empresa.DoesNotExist:
            return Response({'error': 'Empresa no encontrada'}, status=status.HTTP_404_NOT_FOUND)
        
        empresa.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
```        

### USO DE VIEWSETS
```
class EmpresaVS(ViewSet):
    def list(self, request):
        # devuelve la lista de todas las empresas
        queryset = Empresa.objects.all()
        serializer = EmpresaSerializer(queryset, many=True)
        return Response(serializer.data)
    
    def retrieve(self, request, pk=None):
        queryset = Empresa.objects.all()
        edificacionList = get_object_or_404(queryset, pk=pk)
        serializer = EmpresaSerializer(edificacionList)
        return Response(serializer.data)
```